package main

import (
	"fmt"
	"os"
	"strconv"
)

var basePath = "/sys/class/power_supply/"

func readPath(path string) string {
	file, err := os.Open(path)
	if err != nil {
		return ""
	}
	defer func() {
		if err = file.Close(); err != nil {
			fmt.Println(err)
		}
	}()

	data := make([]byte, 16)
	b, err := file.Read(data)
	if err != nil {
		return ""
	}

	return string(data[0 : b-1])
}

func main() {
	// read charger attached
	chargerAttached := readPath(basePath + "AC/online")

	// read battery present
	batteryPresent := readPath(basePath + "BAT0/present")

	// charger is attached and battery present, we can exit early
	if chargerAttached == "1" && batteryPresent == "1" {
		return
	}

	exitCode := 0

	// read /etc/check-power/allowed-battery-level
	allowedLevelStr := readPath("/etc/check-power/allowed-battery-level")
	allowedLevel, err := strconv.ParseInt(allowedLevelStr, 10, 64)
	if err != nil {
		fmt.Printf("Error reading allowed battery level from /etc/check-power/allowed-battery-level: %v\n", err)
	}

	// read current battery level
	currentLevelStr := readPath(basePath + "BAT0/capacity")
	currentLevel, err := strconv.ParseInt(currentLevelStr, 10, 64)
	if err != nil {
		fmt.Printf("Error reading battery level: %v\n", err)
	}

	if currentLevel < allowedLevel {
		fmt.Printf("Charger attached: %s, BatteryPresent: %s, Battery level: %d, Battery level allowed: %d\n", chargerAttached, batteryPresent, currentLevel, allowedLevel)
		os.Exit(1)
	}

	if exitCode != 0 {
	}
}
