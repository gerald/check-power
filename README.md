# check-power

Simple utility to check if a laptop running linux (>= 4.17) is connected to a power source.

Tested on a X230 and T440p

## Usage

The program reads from `/sys/class/power_supply` to determine battery status.

You also need to create a file containing a percentage at `/etc/check-power/allowed-battery-level`. This level will be considered as if the laptop would be still attached to a charger.

```
mkdir -p /etc/check-power
echo 50 > /etc/check-power/allowed-battery-level
```

Now run the program. The following table shows possible exit code scenarios:

| Charger attached | Battery present | Battery level below threshold | Exit code |
|     ---          |     ---         |                    ---        |     ---    |
| Yes | Yes | No | `0` |
| No  | Yes | No | `0` |
| No  | Yes | Yes | `1` |
| Yes | No  | -   | `1` |

Note that a missing `/etc/check-power/allowed-battery-level` file is considered as a battery level below the threshold.

## Compilation

This program can be compiled either using the normal go compiler (`go build`) or [tinygo].

When compiled with [tinygo], use `CGO_ENABLED=0 tinygo build -no-debug -ldflags '-w -s' -o check-power main.go` to create a very small binary. (72k vs 1.4M)

[tinygo]: https://tinygo.org

## License

[MIT](LICENSE)
